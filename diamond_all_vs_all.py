#!/usr/bin/env python3

# This script assumes the following project structure (auto created if missing)
# ./data
# ./data/split
# ./logs
# ./results
#
# NOTE Edit DNA below pointing to the file containing the sequences.
#      The filename should end in .fna

from __future__ import print_function
from itertools import product
from jug import TaskGenerator, bvalue
from pathlib import Path
from subprocess import run
import os
import re


INPUT = Path("data/freeze11.genes.fna")
BIN = Path("software")

CORES = os.environ.get("Q_CORES", "1")
TMPDIR = os.environ.get("TMPDIR", "/tmp")

LOGS = Path("logs")
RESULTS = Path("results")
DATA = Path("data")
SPLIT = DATA / "split"

DNA_SUFFIX = ".fna"
PROTEIN_SUFFIX = ".faa"

DIAMOND = BIN / "diamond" / "0.8.36" / "diamond"
FNA2FAA = BIN / "fna2faa" / "0.0.2" / "fna2faa"
FASTASPLIT = BIN / "fastasplit" / "0.0.1" / "fastasplit"


def logfile_from_path(*paths, prefix="path"):
    path = "_".join(p.stem for p in paths)

    return LOGS / "{}_{}.log".format(prefix, path)


def prechecks():
    """Ensure necessary folders and inputs exist before starting"""

    LOGS.mkdir(exist_ok=True)
    RESULTS.mkdir(exist_ok=True)
    DATA.mkdir(exist_ok=True)
    SPLIT.mkdir(exist_ok=True)
    assert INPUT.is_file(), "ERROR: {} doesn't exist or is not a file".format(INPUT)


def run_paths(cmd, **kw):
    run(list(map(str, cmd)), **kw)


@TaskGenerator
def dna_to_protein(dna, force_update=False):
    protein = dna.with_suffix(PROTEIN_SUFFIX)

    if protein.is_file() and not force_update:
        print("WARNING: Protein sequence already exists. Skipping translation")
        return protein

    with protein.open('wb') as out:
        run_paths([FNA2FAA, dna], stdout=out, check=True)

    return protein


@TaskGenerator
def split_sequences(seq, suffix):
    seq_split = (SPLIT / INPUT.name).with_suffix(suffix)
    log_path = logfile_from_path(seq, prefix="sequence_fastasplit")

    with log_path.open('wb') as log:
        run_paths([FASTASPLIT, seq, seq_split],
                  stdout=log, stderr=log, check=True)

    return list(SPLIT.glob("{}.*{}".format(seq.stem, suffix)))


@TaskGenerator
def make_diamond_db(chunk):
    log_path = logfile_from_path(chunk, prefix="diamond_makedb")

    with log_path.open('wb') as log:
        run_paths([DIAMOND, "makedb", "--in", chunk, "--db", chunk],
                  stdout=log, stderr=log, check=True)

    return chunk


@TaskGenerator
def run_diamond(chunk, chunkdb):
    index_match = re.compile(".*\.(\d+)\..*")
    chunk_id = index_match.match(str(chunk)).groups()[0]
    chunkdb_id = index_match.match(str(chunkdb)).groups()[0]
    identifier = "q{}_d{}".format(chunk_id, chunkdb_id)

    out = RESULTS / "{}.{}.txt".format(INPUT.stem, identifier)

    cmd = [DIAMOND, "blastx", "-p", str(CORES), "-f", "6",
           "--query", chunk, "--db", chunkdb, "--out", out, "-t", TMPDIR]

    log_path = logfile_from_path(out, prefix="diamond_run")

    with log_path.open('wb') as log:
        run_paths(cmd, stdout=log, stderr=log, check=True)


prechecks()
protein = dna_to_protein(INPUT)

dna_chunks = bvalue(split_sequences(INPUT, DNA_SUFFIX))
prot_chunks = bvalue(split_sequences(protein, PROTEIN_SUFFIX))

prot_dbs = map(make_diamond_db, prot_chunks)

for dna, prot_db in product(dna_chunks, prot_dbs):
    run_diamond(dna, prot_db)
