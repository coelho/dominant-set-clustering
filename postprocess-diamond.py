from datetime import datetime
import gzip
import networkx as nx
import networkx.utils.union_find
from collections import defaultdict
import multiprocessing
import pathlib
from glob import glob

from jug import TaskGenerator, barrier
from jug.utils import jug_execute
import os


import pair_compare

INPUT_GLOB = '/g/scb/bork/ralves/projects/cataclust/results/freeze11.genes.clean.*.txt'
OUTPUT_CATALOG  = './freeze11.95nr.fna'

N_CPUS = int(os.getenv('LSB_MAX_NUM_PROCESSORS', 1))

@TaskGenerator
def check_file(fname, ofname):
    pair_compare.load_data()
    with multiprocessing.Pool(N_CPUS) as p, \
            open(ofname, 'w') as output, \
            open(fname) as ifile:
        grouped = pair_compare.groups_of(ifile, 100)
        for qs in p.imap(pair_compare.compare_lines, grouped):
            for q0, q1 in qs:
                output.write("{}\t{}\n".format(q0, q1))

res = {}
ifiles = [pathlib.PurePath(p) for p in glob(INPUT_GLOB)]
for ifile in ifiles:
    ofile = pathlib.PurePath('outputs/') / ifile.name
    t = check_file(ifile, ofile)
    res[ofile] = t
    res[str(ofile) + '.gz'] = jug_execute(['gzip', ofile], run_after=t)

barrier()

@TaskGenerator
def union(ifiles, ofile):
    uf = networkx.utils.union_find.UnionFind()
    bad_files = []
    for i,ifile in enumerate(ifiles):
        with gzip.open(ifile, 'rt') as ifile:
            for line in ifile:
                toks = line.strip().split()
                if len(toks) == 2:
                    [v0, v1] = toks
                    uf.union(v0, v1)
        if (i+1) % 5 == 0:
            print("File {}/{} done [{:.1%}] at {}.".format(
                                i + 1, n, (i+1.)/n,
                                datetime.now().strftime('%Y-%m-%d %Hh%M.%S')))

    rev = defaultdict(list)
    for elem in uf:
        rev[uf[elem]].append(elem)

    with gzip.open(ofile, 'wt') as output:
        for vs in rev.values():
            output.write("\t".join(vs))
            output.write('\n')
    return bad_files

@TaskGenerator
def merge_all(partials, ofile):
    uf = networkx.utils.union_find.UnionFind()

    for p in partials:
        with gzip.open(p, 'rt') as ifile:
            for line in ifile:
                tokens = line.strip().split('\t')
                uf.union(*tokens)
    rev = defaultdict(list)
    for elem in uf:
        rev[uf[elem]].append(elem)

    with gzip.open(ofile, 'wt') as output:
        for vs in rev.values():
            output.write("\t".join(vs))
            output.write('\n')

ifiles = glob('outputs/freeze11.*.gz')
ifiles.sort()
n = len(ifiles)

partials = []
bads = []
for bi in range(n//100 + bool(n % 100)):
    ofile = 'outputs/union-find.{}.gz'.format(bi)
    bads.append(union(ifiles[bi*100:(bi+1)*100], ofile))
    partials.append(ofile)

barrier()
merge_all(partials, 'outputs/union.find/final.gz')

barrier()

PERIOD = 500
N_BLOCKS = 100

@TaskGenerator
def shard(block):
    indices = {}
    for i,line in enumerate(gzip.open('outputs/union.find/final.gz', 'rt')):
        index = i % PERIOD
        toks = line.strip().split()
        for t in toks:
            indices[t] = index
    outputs = [gzip.open('outputs/splits/{}.{}.gz'.format(block, i), 'wt') for i in range(PERIOD + 1)]
    ifiles = glob('outputs/freeze*.txt.gz')
    n = len(ifiles)
    for i,ifile in enumerate(ifiles):
        if i % N_BLOCKS != block:
            continue
        for line in gzip.open(ifile, 'rt'):
            toks = line.strip().split()
            outputs[indices.get(toks[0], PERIOD)].write(line)
        print("Done {}/{}".format(i + 1, n))

for j in range(N_BLOCKS):
    shard(j)


class ThinGraph(nx.Graph):
    all_edge_dict = {'weight': 1}
    def single_edge_dict(self):
        return self.all_edge_dict
    edge_attr_dict_factory = single_edge_dict


def process(i, G, qi, qo):
    while True:
        t = qi.get()
        if t is None:
            print("Worker bee [{}] exiting...".format(i))
            qi.task_done()
            return
        print("Worker bee [{}] starting.".format(i))
        Gi = G.subgraph(t)
        vs = nx.algorithms.dominating_set(Gi)
        qo.put(vs)
        qi.task_done()
        print("Worker bee [{}] finished 1.".format(i))


@TaskGenerator
def build_catalog(index):
    G = ThinGraph()
    relevant = glob('outputs/splits/*.{}.gz'.format(index))
    relevant.sort()
    G = ThinGraph()
    for r in relevant:
        for line in gzip.open(r, 'rt'):
            # [v0,v1] = line.strip().split('\t')
            toks = line.strip().split('\t')
            if len(toks) == 2:
                [v0, v1] =toks
                G.add_edge(v0, v1)
        print("Loaded {}".format(r))

    catalog = []
    for Gi in nx.connected_components(G):
        Gi = G.subgraph(Gi)
        vs = nx.algorithms.dominating_set(Gi)
        catalog.extend(vs)
    with gzip.open('outputs/catalog/partial.{}.gz'.format(index), 'wt') as output:
        for v in catalog:
            output.write(v)
            output.write('\n')
        output.flush()
        os.fsync(output.fileno())
barrier()
for j in range(PERIOD):
    build_catalog(j)

barrier()
@TaskGenerator
def write_catalog():
    import skbio.io
    in_catalog = set()
    for g in sorted(glob('outputs/catalog/partial.*.gz')):
        for line in gzip.open(g, 'rt'):
            in_catalog.add(line.strip())
    with open(OUTPUT_CATALOG, 'wt') as output:
        for seq in skbio.io.read('freeze11.genes.clean99.fna', format='fasta'):
            if seq.metadata['id'] in in_catalog:
                seq.write(output)
write_catalog()
