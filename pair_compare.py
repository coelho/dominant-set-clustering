_data = None
def load_data():
    global _data
    if _data is not None:
        return _data
    import mmap
    import pandas as pd
    fai = pd.read_table('freeze11.genes.fa.fai', index_col=0, header=None,
            names=['NAME', 'LENGTH', 'OFFSET', 'LINEBASES', 'LINEWIDTH'])
    index = {}
    for i,v in enumerate(fai.index):
        index[v] = i
    faidata = fai.values[:,:2]
    fafile = open('freeze11.genes.fa', 'rb')
    fadata = mmap.mmap(fafile.fileno(), 0, access=mmap.ACCESS_READ)
    _data = index, faidata, fadata
    return index, faidata, fadata




def compare_pair(q0, q1, data):
    import skbio.sequence
    import skbio.alignment
    index, faidata, fadata = data
    n0,p0 = faidata[index.get(q0)]
    n1,p1 = faidata[index.get(q1)]
    seq0 = skbio.sequence.DNA(fadata[p0:p0+n0])
    seq1 = skbio.sequence.DNA(fadata[p1:p1+n1])
    if len(seq1) > len(seq0):
        seq0,seq1 = seq1,seq0
    # Default is match equals 2 points & mismatch -3
    # 95% ID over 90% : len * 90% * (95% * 2 + 5% * (-3))
    #                 = len * 0.9 * (1.9 - .15)
    #                 = len * 0.9 * 1.75
    #                 = len * 1.575
    # Finally, it needs to fit into an uint16, so set a limit at 65535 (= 2**16-1)
    min_score = min(65535, len(seq1) * 0.9 * 1.75)
    matched = skbio.alignment.local_pairwise_align_ssw(seq0, seq1, score_filter=min_score)
    if matched is not None:
        _, score, pos = matched
        len_hit = pos[1][1]-pos[1][0]
        if len_hit >= len(seq1) * .9 and score > len_hit * 1.75:
            return True
    return False


def compare_lines(lines):
    data = load_data()
    res = []
    for line in lines:
        if line is None:
            break
        q0,q1 = line.strip().split('\t')[:2]
        if q0 != q1 and compare_pair(q0, q1, data):
            res.append((q0,q1))
    return res

def groups_of(iterable, n):
    """Collect data into fixed-length chunks or blocks

    Example:
        groups_of('ABCDEFG', 3) --> ABC DEF G"
    """
    cur = []
    for it in iterable:
        cur.append(it)
        if len(cur) == n:
            yield cur
            cur = []
    if cur:
        yield cur
